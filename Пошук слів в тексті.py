"""
Знайти в тексті слова які співпадають
"""

text = """It was dark when we arrived at Dr Huxtable's famous school in the northern hills. We went quickly into the building out of the cold, and at once someone ran up with news for Dr Huxtable.
He looked very surprised. 'The Duke is here,' he told us. 'The Duke and Mr Wilder, his secretary, are in my office. Come and meet them.'
The Government Minister was a tall man with a long, thin face. He had red hair, and a great red beard. He looked at us, and did not smile. Next to him stood Mr Wilder, a very young man. He was small, with blue eyes, and a watchful face. He spoke first.
'The Duke is surprised, Dr Huxtable, to see Mr Sherlock Holmes here. He doesn't want people to know about this. You know that, so why didn't you speak to the Duke before you went to London?'
'But we need help,' said Dr Huxtable. 'And I -'
'Well,' said the Duke. 'Mr Holmes is here now, and perhaps he can help us.' He looked at Holmes. 'I'd like you to come and stay at Holdernesse Hall, Mr Holmes.'
'Thank you, sir,' said Holmes. 'But I would like to stay near to the mystery, here at the school. Can I ask you one or two questions, perhaps?'
'Of course,' said the Duke.
'My questions are about the Duchess, and about money,' said Holmes."""


for char in "['!*+-.?!,']":
    text = text.replace(char, '')

text = text.lower()

text_1 = set(text.split())

for word in text_1:
    print(f'{word}: {text.count(word)}')


print("test")






