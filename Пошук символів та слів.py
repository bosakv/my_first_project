"""
Знайти в тексті символи та слова які співпадають
"""
text = """It was dark when we arrived at Dr Huxtable's famous school in the northern hills. We went quickly into the building out of the cold, and at once someone ran up with news for Dr Huxtable.
He looked very surprised. 'The Duke is here,' he told us. 'The Duke and Mr Wilder, his secretary, are in my office. Come and meet them.'
The Government Minister was a tall man with a long, thin face. He had red hair, and a great red beard. He looked at us, and did not smile. Next to him stood Mr Wilder, a very young man. He was small, with blue eyes, and a watchful face. He spoke first.
'The Duke is surprised, Dr Huxtable, to see Mr Sherlock Holmes here. He doesn't want people to know about this. You know that, so why didn't you speak to the Duke before you went to London?'
'But we need help,' said Dr Huxtable. 'And I -'
'Well,' said the Duke. 'Mr Holmes is here now, and perhaps he can help us.' He looked at Holmes. 'I'd like you to come and stay at Holdernesse Hall, Mr Holmes.'
'Thank you, sir,' said Holmes. 'But I would like to stay near to the mystery, here at the school. Can I ask you one or two questions, perhaps?'
'Of course,' said the Duke.
'My questions are about the Duchess, and about money,' said Holmes."""

eng_alphabet = "a b c d e f g h i j k l m n o p q r s t u v w x y z"
no_white_space = eng_alphabet.replace(" ", "")
alphabet = list(no_white_space)
alpha_count_dic = {}

for letter in text.lower():
    if letter in alphabet:
        alpha_count_dic.setdefault(letter,0)
        alpha_count_dic[letter] +=1

for key, value in sorted(alpha_count_dic.items()):
    print (key,"occrus:", value, "times")



